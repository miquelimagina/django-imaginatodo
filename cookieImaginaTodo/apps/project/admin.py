from django.contrib import admin
from .models import Tag, Tarea, Project


class TagInline (admin.TabularInline):
    model = Tag


class TareaInline (admin.TabularInline):
    model = Tarea


@admin.register(Tag)
class TagAdmin (admin.ModelAdmin):
    list_display = [
        'titulo', 'creado_en']
    search_fields = ['titulo']
    ordering = ['-creado_en']


@admin.register(Tarea)
class TareaAdmin (admin.ModelAdmin):
    list_display = [
        'titulo', 'estado']
    search_fields = ['titulo']
    list_filter = ['estado']
    ordering = ['-creado_en']
    # inlines = [TagInline]


@admin.register(Project)
class ProjectAdmin (admin.ModelAdmin):
    list_display = [
        'titulo', 'fecha_final']
    search_fields = ['titulo']
    list_filter = ['creado_en', 'tags']
    ordering = ['-creado_en']
    # inlines = [TagInline,TareaInline ]
