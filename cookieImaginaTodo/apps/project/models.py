from django.db import models
from django.contrib.auth.models import User
from django.core.urlresolvers import reverse
import datetime


class Tag (models.Model):
    titulo = models.CharField(max_length=50, unique=True)
    creado_en = models.DateTimeField(
        'creado en', default=datetime.datetime.now)

    def get_absolute_url(self):
        return reverse('tag-detail', kwargs={'pk': self.pk})

    def __str__(self):
        return self.titulo


class Tarea(models.Model):
    ESTADO_TAREA = (
        ('Todo', 'Todo'),
        ('Doing', 'Doing'),
        ('Done', 'Done'),
    )
    titulo = models.CharField(max_length=80)
    descripcion = models.CharField(max_length=140)
    estado = models.CharField(max_length=5, choices=ESTADO_TAREA)
    tags = models.ManyToManyField(Tag)
    creado_por = models.ForeignKey(
        User, related_name='tarea_creada_por')
    actualizado_por = models.ForeignKey(
        User, null=True, blank=True, related_name='tarea_actualizada_por')
    fecha_final = models.DateTimeField('fecha final', null=True)
    creado_en = models.DateTimeField(
        'creado en', default=datetime.datetime.now)
    actualizado_en = models.DateTimeField('actualizado en', null=True,  blank=True)

    def get_absolute_url(self):
        return reverse('tarea-detail', kwargs={'pk': self.pk})

    def __str__(self):
        return self.titulo + " completa:" + self.estado


class Project(models.Model):
    titulo = models.CharField(max_length=80, unique=True)
    descripcion = models.CharField(max_length=140)
    tags = models.ManyToManyField(Tag)
    tareas = models.ManyToManyField(Tarea)
    creado_por = models.ForeignKey(User, related_name='proyecto_creado_por')
    actualizado_por = models.ForeignKey(
        User, null=True, blank=True, related_name='proyecto_actualizado_por')
    fecha_final = models.DateTimeField('fecha final', null=True)
    creado_en = models.DateTimeField(
        'creado en', default=datetime.datetime.now)
    actualizado_en = models.DateTimeField('actualizado en', null=True, blank=True)

    def get_absolute_url(self):
        return reverse('project-detail', kwargs={'pk': self.pk})

    def __str__(self):
        return self.titulo
