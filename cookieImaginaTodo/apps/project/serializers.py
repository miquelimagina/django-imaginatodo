from django.contrib.auth.models import User
from .models import Tag, Tarea, Project
from rest_framework import serializers


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('username', 'email')


class TagSerializer(serializers.ModelSerializer):
    class Meta:
        model = Tag
        fields = ('id', 'titulo')


class TareaSerializer(serializers.ModelSerializer):
    tags = TagSerializer(many=True)
    creado_por = UserSerializer(many=False)
    actualizado_por = UserSerializer(many=False)

    class Meta:
        model = Tarea
        fields = '__all__'


class ProjectSerializer(serializers.ModelSerializer):
    tags = TagSerializer(many=True)
    tareas = TareaSerializer(many=True)
    creado_por = UserSerializer(many=False)
    actualizado_por = UserSerializer(many=False)

    class Meta:
        model = Project
        fields = '__all__'
