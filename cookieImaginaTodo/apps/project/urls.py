from django.conf.urls import url
from project.views import *


# Wire up our API using automatic URL routing.
# Additionally, we include login URLs for the browsable API.
urlpatterns = [
    url(r'^tags/$', TagList.as_view(), name='tag-list'),
    url(r'^tags/(?P<pk>[0-9]+)/$', TagDetail.as_view(), name='tag-detail'),
    url(r'tag/add/$', TagCreate.as_view(), name='tag-add'),
    url(r'tag/(?P<pk>[0-9]+)/$', TagUpdate.as_view(), name='tag-update'),
    url(r'tag/(?P<pk>[0-9]+)/delete/$',
        TagDelete.as_view(), name='tag-delete'),

    url(r'^tareas/$', TareaList.as_view(), name='tarea-list'),
    url(r'^tareas/(?P<pk>[0-9]+)/$',
        TareaDetail.as_view(), name='tarea-detail'),
    url(r'tarea/add/$', TareaCreate.as_view(), name='tarea-add'),
    url(r'tarea/(?P<pk>[0-9]+)/$', TareaUpdate.as_view(), name='tarea-update'),
    url(r'tarea/(?P<pk>[0-9]+)/delete/$',
        TareaDelete.as_view(), name='tarea-delete'),

    url(r'^projects/$', ProjectList.as_view(), name='project-list'),
    url(r'^projects/(?P<pk>[0-9]+)/$',
        ProjectDetail.as_view(), name='project-detail'),
    url(r'project/add/$', ProjectCreate.as_view(), name='project-add'),
    url(r'project/(?P<pk>[0-9]+)/$',
        ProjectUpdate.as_view(), name='project-update'),
    url(r'project/(?P<pk>[0-9]+)/delete/$',
        ProjectDelete.as_view(), name='project-delete'),
]
