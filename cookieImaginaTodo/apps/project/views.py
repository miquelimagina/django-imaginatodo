from django.contrib.auth.models import User
from .models import Tag, Tarea, Project
from django.contrib.messages.views import SuccessMessageMixin
from rest_framework import viewsets
from project.serializers import UserSerializer, TagSerializer
from project.serializers import TareaSerializer, ProjectSerializer
from django.views.generic import ListView, DetailView, TemplateView
from django.views.generic import CreateView, UpdateView, DeleteView
from django.contrib.auth.decorators import login_required, permission_required
from django.utils.decorators import method_decorator
from django.contrib import messages
from django.core.urlresolvers import reverse_lazy
from django.db.models import Count
# from django.contrib.auth import authenticate, login, logout
# from django.core.urlresolvers import reverse
import datetime

# #####################################
# RESTful
# #####################################


class UserViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = User.objects.all().order_by('-date_joined')
    serializer_class = UserSerializer


class TagViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = Tag.objects.all().order_by('-creado_en')
    serializer_class = TagSerializer


class TareaViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = Tarea.objects.all().order_by('-creado_en')
    serializer_class = TareaSerializer


class ProjectViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = Project.objects.all().order_by('-creado_en')
    serializer_class = ProjectSerializer


# #####################################
# HTML
# #####################################
class IndexView(TemplateView):
    template_name = "index.html"


class TagList(ListView):
    model = Tag


class TagDetail(DetailView):

    model = Tag

    def get_context_data(self, **kwargs):
        # Call the base implementation first to get a context
        context = super(TagDetail, self).get_context_data(**kwargs)
        # Add in a QuerySet of all the books
        context['tarea_list'] = Tarea.objects.all()
        context['project_list'] = Project.objects.all()
        return context


@method_decorator(login_required, name='dispatch')
class TagCreate(SuccessMessageMixin, CreateView):
    model = Tag
    fields = ['titulo']
    success_message = "%(titulo)s creada correctamente"

    def form_valid(self, form):
        form.instance.creado_por = self.request.user
        return super(TagCreate, self).form_valid(form)


@method_decorator(login_required, name='dispatch')
class TagUpdate(SuccessMessageMixin, UpdateView):
    model = Tag
    fields = ['titulo']
    success_message = "%(titulo)s actualizada correctamente"


@method_decorator(permission_required('tag.delete_tag'), name='dispatch')
class TagDelete(SuccessMessageMixin, DeleteView):
    model = Tag
    success_message = "tag eliminada correctamente"
    success_url = reverse_lazy('tag-list')

    def delete(self, request, *args, **kwargs):
        messages.success(self.request, self.success_message)
        return super(TagDelete, self).delete(request, *args, **kwargs)


class TareaList(ListView):
    model = Tarea


class TareaDetail(DetailView):

    model = Tarea


@method_decorator(login_required, name='dispatch')
class TareaCreate(SuccessMessageMixin, CreateView):
    model = Tarea
    fields = ['titulo', 'descripcion', 'tags', 'estado', 'fecha_final']
    success_message = "%(titulo)s creada correctamente"

    def form_valid(self, form):
        form.instance.creado_por = self.request.user
        return super(TareaCreate, self).form_valid(form)


@method_decorator(login_required, name='dispatch')
class TareaUpdate(SuccessMessageMixin, UpdateView):
    model = Tarea
    fields = ['titulo', 'descripcion', 'tags', 'estado', 'fecha_final']
    success_message = "%(titulo)s actualizada correctamente"

    def form_valid(self, form):
        form.instance.actualizado_por = self.request.user
        form.instance.actualizado_en = datetime.datetime.now()
        return super(TareaUpdate, self).form_valid(form)


@method_decorator(permission_required('tarea.delete_tarea'), name='dispatch')
class TareaDelete(SuccessMessageMixin, DeleteView):
    model = Tarea
    success_message = "tarea eliminada correctamente"
    success_url = reverse_lazy('tarea-list')

    def delete(self, request, *args, **kwargs):
        messages.success(self.request, self.success_message)
        return super(TareaDelete, self).delete(request, *args, **kwargs)


class ProjectList(ListView):
    model = Project

    def get_queryset(self):
        # Intentar encontrar la forma de contar el numero de noticias por
        # Autor
        return Project.objects.annotate(Count("tareas"))


class ProjectDetail(DetailView):

    model = Project


@method_decorator(login_required, name='dispatch')
class ProjectCreate(SuccessMessageMixin, CreateView):
    model = Project
    fields = ['titulo', 'descripcion', 'tareas',
              'fecha_final', 'tags']
    success_message = "%(titulo)s creado correctamente"

    def form_valid(self, form):
        form.instance.creado_por = self.request.user
        return super(ProjectCreate, self).form_valid(form)


@method_decorator(login_required, name='dispatch')
class ProjectUpdate(SuccessMessageMixin, UpdateView):
    model = Project
    fields = ['titulo', 'descripcion', 'tags', 'estado']
    success_message = "%(titulo)s actualizado correctamente"

    def form_valid(self, form):
        form.instance.actualizado_por = self.request.user
        form.instance.actualizado_en = datetime.datetime.now()
        return super(ProjectUpdate, self).form_valid(form)


@method_decorator(permission_required('project.delete_project'), name='dispatch')
class ProjectDelete(SuccessMessageMixin, DeleteView):
    model = Project
    success_message = "%(titulo)s eliminado correctamente"
    success_url = reverse_lazy('tarea-list')

    def delete(self, request, *args, **kwargs):
        messages.success(self.request, self.success_message)
        return super(ProjectDelete, self).delete(request, *args, **kwargs)
