from django.conf.urls import url, include
from project.views import IndexView
from django.contrib import admin

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^api/', include('project.apiendpoints')),
    url(r'^accounts/login/$', 'django.contrib.auth.views.login', {'template_name': 'admin/login.html'},name='login'),
    url(r'^accounts/logout/$', 'django.contrib.auth.views.logout', name='logout'),
    url(r'^', include('project.urls')),
    url(r'^', IndexView.as_view(), name='index'),
]
